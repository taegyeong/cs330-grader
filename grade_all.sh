#!/bin/bash

# (root)
#  ├─grade_all.sh
#  └─files/
#     ├─grading.sh
#     ├─1.tar.gz
#     ├─2.tar.gz
#        ...
#     └─41.tar.gz

trial=2

teams=$(seq 1 41)

# dual submission
for team in $teams; do
    teams+=" _$team"
done

pushd files
for (( i=0; i<$trial; i++))
do
    ./grading.sh vm

    mkdir -p ../grades/trial_$i
    cp grade/* ../grades/trial_$i/

    rm -rf $teams
    rm -rf grade
done
popd