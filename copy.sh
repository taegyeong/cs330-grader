vms="cs330-1 cs330-2 cs330-3 cs330-4"
grading_dir=grading_pj3_1

mkdir -p grade_files
for vm in $vms
do
    # ssh $vm "ls $grading_dir/grades"
    scp -r $vm:$grading_dir/grades/ grade_files/$vm
done