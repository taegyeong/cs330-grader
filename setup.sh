vms="cs330-1 cs330-2 cs330-3 cs330-4"
grading_dir=grading_pj3_1

for vm in $vms
do
    ssh $vm "mv pintos _pintos"
    scp pintos.tar $vm:./
    ssh $vm "tar -xvf pintos.tar"
    ssh $vm "rm pintos.tar"

    ssh $vm "mkdir -p $grading_dir"
    ssh $vm "mkdir -p $grading_dir/files"

    scp grade_all.sh $vm:$grading_dir/
    scp grading.sh $vm:$grading_dir/files/
    scp renamed_files/* $vm:$grading_dir/files

    ssh $vm "chmod +x $grading_dir/grade_all.sh"
    ssh $vm "chmod +x $grading_dir/files/grading.sh"
done