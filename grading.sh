#!/bin/bash

# Prerequisites
# -------------
# Make sure that the *original pintos* is placed on a home directory. This program copies test codes from it.
# 'pintos' executable (from src/utils) should be in the path.

# Usage
# -----
# First, put project archives (.zip or .tar.gz) inside the root directory (where run.sh will be executed). For convenience, you can change the name of archives to team numbers, but not necessary.
# Second, execute run.sh with options, either a project directory or a project number (e.g., threads or 1).
# It automatically generates directories of each archive and copies the archives to the corresponding directories.
# It then make-grades in each directory and copies the grade result to the 'grade' directory. Its name is automatically changed to the archive name.

# variables
shopt -s nullglob
pjNames=(threads userprog vm filesys)
pjNums=(1 2 3 4)
hasPintos=1

# clean
if [ "$#" -eq 1 ]
then
	if [ $1 == clean ]
	then
		for f in *; do
			if [ -d $f ]
			then
				rm -rf $f
			fi
		done
		exit 1
	fi
fi

# target directory (or project)
targetDir=threads
if [ "$#" -ne 1 ]
then
	echo -e "Usage:\t./grading.sh {threads/userprog/vm/filesys/1/2/3/4/clean}"
	exit 1
fi
for i in ${pjNames[@]}; do
	if [ $1 == $i ]
	then
		targetDir=$1
	fi
done
for i in ${pjNums[@]}; do
	if [ $1 == $i ]
	then
		targetDir=${pjNames[$((i-1))]}
	fi
done
echo -e "Target directory is: $targetDir"

# create directories
for f in *.*; do
	if [ ! -d ${f%%.*} ] && [ ! ${f: -3} == ".sh" ]
	then
		mkdir ${f%%.*}
    cp $f ${f%%.*}
	fi
done

# create makeGrade directory
mkdir grade

# loop every folder
for dir in *; do
	if [ -d $dir ]
	then
		if [ ! $dir == "grade" ]
		then
			cd $dir

			# unzip first
			for f in *zip; do
				unzip "$f"
			done

			# extract pintos directory
			for f in *tar* *gz; do
				tar -xf "$f"
			done

			# if the extracted directory root is pintos, enter
			hasPintos=0
			if [ -d pintos ]
			then
				hasPintos=$((hasPintos+1))
				cd pintos
			fi

			cd src
			# Due to character format diff between Windows and Linux, change tests with the original one.
			rm -rf tests/
			cp -R ~/pintos/src/tests ./
			cd $targetDir
			make clean
			make grade

			# copy the result to grade directory
			if [ $hasPintos == 1 ]
			then
				cp build/grade ../../../../grade/$dir
				cd ../../../../
			else
				cp build/grade ../../../grade/$dir
				cd ../../../
			fi
		fi
	fi
done
