import os
import shutil

KLMS_PATH = 'klms_files'
OUT_PATH = 'renamed_files'

file_list = os.listdir(KLMS_PATH)
file_list = [file for file in file_list if file.endswith(".tar.gz")]

os.makedirs(OUT_PATH, exist_ok=True)

new_files = []

for f_name in file_list:
    new_name = f_name.split('_')[-1]
    if new_name in new_files:
        new_name = '_' + new_name
    new_files.append(new_name)
    new_path = os.path.join(OUT_PATH, new_name)
    old_path = os.path.join(KLMS_PATH, f_name)
    shutil.copy(old_path, new_path)

