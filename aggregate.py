import os
import numpy as np
import pandas as pd

PATH_GRADE = 'grade_files'
PATH_SUBMITTED = 'renamed_files'
CSV_OUT = "grade.csv"

HEADER_SCORE = "SUMMARY BY TEST SET"

POINT_BOUND = [35, 15, 108, 88, 30] # only for pj3-1
POINT_MAX = [55, 28, 108, 88, 30]
SCORE_MAX = [50., 15., 10., 5., 20.]

"""
SUMMARY BY TEST SET

Test Set                                      Pts Max  % Ttl  % Max
--------------------------------------------- --- --- ------ ------
tests/vm/Rubric.functionality                  35/ 55  31.8%/ 50.0%
tests/vm/Rubric.robustness                     15/ 28   8.0%/ 15.0%
tests/userprog/Rubric.functionality           108/108  10.0%/ 10.0%
tests/userprog/Rubric.robustness               88/ 88   5.0%/  5.0%
tests/filesys/base/Rubric                      30/ 30  20.0%/ 20.0%
--------------------------------------------- --- --- ------ ------
Total                                                  74.9%/100.0%
"""

NUM_FIELDS = len(SCORE_MAX) + 1
vms = os.listdir(PATH_GRADE)

path_list = []
for vm in vms:
    trials = os.listdir(os.path.join(PATH_GRADE, vm))
    path_list += [os.path.join(PATH_GRADE, vm, trial) for trial in trials]

teams = [
    f.split('.')[0]
    for f in os.listdir(PATH_SUBMITTED) if f.endswith(".tar.gz")
]

def parse_point(score_text):
    pts = score_text.strip().split()[1:-2]
    pt, pt_max = [int(p) for p in "".join(pts).split('/')]
    return pt

def read_score(f_path):
    global HEADER_SCORE
    global POINT_BOUND, POINT_MAX, SCORE_MAX

    if not os.path.isfile(f_path):
        return [0.] * NUM_FIELDS
    with open(f_path, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                return [0.] * NUM_FIELDS
            line = line.strip()
            if line == HEADER_SCORE:
                break
            
        score_texts = f.readlines()[3:3 + len(SCORE_MAX)]
        points = [parse_point(score_text) for score_text in score_texts]
        points = np.amin([POINT_BOUND, points], axis=0)
        score = np.multiply(np.divide(points, POINT_MAX), SCORE_MAX)
        score = np.append([sum(score)], score)
        score = np.round_(score, 1)
        return score

def get_scores(team, path_list):
    scores = [read_score(os.path.join(path, team)) for path in path_list]
    return list(np.array(scores).flatten())

columns = [''] + [
    "trial {:02d}".format(i + 1) if j == 0 else ''
    for i, _ in enumerate(path_list) for j in range(NUM_FIELDS)
]
scores = [[team] + get_scores(team, path_list) for team in teams]

df = pd.DataFrame(scores, columns=columns)
df.to_csv(CSV_OUT, index=False)